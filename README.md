# RF Monitor Project

The RF Monitor Project offers a comprehensive solution for capturing and analyzing Radio Frequency (RF) spectra using a Raspberry Pi and RF Explorer hardware.

## Folders Overview

- **data**: Contains data files from all RF Monitor tests.
- **doc**: Contains text files related to the final project document. You'll find a PDF named RFExplorer.pdf, which serves as the project's comprehensive documentation, explaining both hardware and software components.
- **images**: Houses plots and visual representations of various tests.
- **scripts**: Includes code utilized for conducting RF spectra measurements. The primary script is named RFE_Freq_2channels.py. Additionally, there's a variant script, RFE_Freq_2channels_timer.py, which includes timing commands to measure the execution time of each script process.