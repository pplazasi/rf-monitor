#pylint: disable=trailing-whitespace, line-too-long, bad-whitespace, invalid-name, R0204, C0200
#pylint: disable=superfluous-parens, missing-docstring, broad-except, R0801
#pylint: disable=too-many-lines, too-many-instance-attributes, too-many-statements, too-many-nested-blocks
#pylint: disable=too-many-branches, too-many-public-methods, too-many-locals, too-many-arguments

#======================================================================================
#This is an example code for RFExplorer python functionality. 
#Display amplitude in dBm and frequency in MHz of the maximum value of frequency range.
#In order to avoid USB issues, connect only RF Explorer Spectrum Analyzer to run this example
#It is not suggested to connect RF Explorer Signal Generator at the same time
#======================================================================================

import time
from datetime import datetime
import RFExplorer
from RFExplorer import RFE_Common 
import numpy as np
import RPi.GPIO as GPIO


#---------------------------------------------------------
# Helper functions
#---------------------------------------------------------

def PrintPeak(objAnalazyer, preindex=None):
    """This function prints the amplitude and frequency peak of the latest received sweep
    """
    
    nIndex = objAnalazyer.SweepData.Count-1
    objSweepTemp = objAnalazyer.SweepData.GetData(nIndex)

    TotalDataPoints = objSweepTemp.TotalDataPoints
    TotalSteps= TotalDataPoints-1
    
    FreqTemp=np.zeros(TotalDataPoints)
    PowerTemp=np.zeros(TotalDataPoints)
    
    for nDataPoint in range(TotalDataPoints):
        fCenterFreq= objSweepTemp.GetFrequencyMHZ(nDataPoint)
        FreqTemp[nDataPoint]=fCenterFreq
        fAmplitudeDBM=objSweepTemp.GetAmplitude_DBM(nDataPoint)
        PowerTemp[nDataPoint]= fAmplitudeDBM
    return FreqTemp, PowerTemp

def ControlSettings(objAnalazyer):
    """This functions check user settings
    """

    SpanSizeTemp = None
    StartFreqTemp = None
    StopFreqTemp =  None

    #print user settings
    print("User settings:" + "Span: " + str(SPAN_SIZE_MHZ) +"MHz"+  " - " + "Start freq: " + str(START_SCAN_MHZ) +"MHz"+" - " + "Stop freq: " + str(STOP_SCAN_MHZ) + "MHz")

    #Control maximum Span size
    if(objAnalazyer.MaxSpanMHZ <= SPAN_SIZE_MHZ):
        print("Max Span size: " + str(objAnalazyer.MaxSpanMHZ)+"MHz")
    else:
        objAnalazyer.SpanMHZ = SPAN_SIZE_MHZ
        SpanSizeTemp = objAnalazyer.SpanMHZ
    if(SpanSizeTemp):
        #Control minimum start frequency
        if(objAnalazyer.MinFreqMHZ > START_SCAN_MHZ):
            print("Min Start freq: " + str(objAnalazyer.MinFreqMHZ)+"MHz")
        else:
            objAnalazyer.StartFrequencyMHZ = START_SCAN_MHZ
            StartFreqTemp = objAnalazyer.StartFrequencyMHZ    
        if(StartFreqTemp):
            #Control maximum stop frequency
            if(objAnalazyer.MaxFreqMHZ < STOP_SCAN_MHZ):
                print("Max Start freq: " + str(objAnalazyer.MaxFreqMHZ)+"MHz")
            else:
                if((StartFreqTemp + SpanSizeTemp) > STOP_SCAN_MHZ):
                    print("Max Stop freq (START_SCAN_MHZ + SPAN_SIZE_MHZ): " + str(STOP_SCAN_MHZ) +"MHz")
                else:
                    StopFreqTemp = (StartFreqTemp + SpanSizeTemp)
    
    return SpanSizeTemp, StartFreqTemp, StopFreqTemp

#---------------------------------------------------------
# global variables and initialization
#---------------------------------------------------------

SERIALPORT = None    #serial port identifier, use None to autodetect 
BAUDRATE = 500000

objRFE = RFExplorer.RFECommunicator()     #Initialize object and thread
objRFE.AutoConfigure = False
SPAN_SIZE_MHZ =  150    #Initialize settings
START_SCAN_MHZ = 50
STOP_SCAN_MHZ = 500


Today= datetime.now()
Now=Today.strftime("%m-%d-%y_%H:%M:%S")
#print(NAMEOUT)

FreqCH=[]
POWER=[[],[]]
Index=[]
PowerCH0=[]
PowerCH1=[]

#---------------------------------------------------------
# Main processing loop
#---------------------------------------------------------
Ti=time.time()

try:
    #Find and show valid serial ports
    objRFE.GetConnectedPorts()

    #Reset IoT board GPIO2 to High Level and GPIO3 to High Level
    objRFE.ResetIOT_HW(True)
    
    

    #Connect to available port
    if (objRFE.ConnectPort(SERIALPORT, BAUDRATE)): 
        #Wait for unit to notify reset completed
        while(objRFE.IsResetEvent):
            pass
            
        #Wait for unit to stabilize
        time.sleep(3)
        
        for i in np.arange(0,2):
            GPIO.setmode(GPIO.BOARD)
            GPIO.setup(32,GPIO.OUT)
            GPIO.output(32,int(i))
        
            if i ==0:
                T1=time.time()
            if i ==1:
                T2=time.time()

            #Request RF Explorer configuration
            objRFE.SendCommand_RequestConfigData()

            #Wait to receive configuration and model details
            while(objRFE.ActiveModel == RFExplorer.RFE_Common.eModel.MODEL_NONE):
                objRFE.ProcessReceivedString(True)    #Process the received configuration
 
            #If object is an analyzer, we can scan for received sweeps
            if(objRFE.IsAnalyzer()):
                
                #Control settings
                SpanSize, StartFreq, StopFreq = ControlSettings(objRFE)
                
                if(SpanSize and StartFreq and StopFreq):

                    while(True):

                        objRFE.UpdateDeviceConfig(StartFreq, StopFreq)
                        objSweep=None
                        
                        #Wait for new configuration to arrive (as it will clean up old sweep data)
                        while(True):
                            objRFE.ProcessReceivedString(True);

                            if (objRFE.SweepData.Count>0):
                                objSweep=objRFE.SweepData.GetData(objRFE.SweepData.Count-1)

                                Freq, Power = PrintPeak(objRFE)
                                
                                if i==0:
                                    PowerCH0.append(Power)
                                    FreqCH.append(Freq)
                                
                                if i==1:
                                    PowerCH1.append(Power)
                                   
                            if(np.fabs(objRFE.StartFrequencyMHZ - StartFreq) <= 0.001):
                                    break        
                        #set new frequency range
                        StartFreq = StopFreq
                        StopFreq = StartFreq + SpanSize
                        if (StopFreq > STOP_SCAN_MHZ):
                            StopFreq = STOP_SCAN_MHZ
                            
                        if (StartFreq >= StopFreq):
                            break
                                
                else:
                    print("Error: settings are wrong.\nPlease, change and try again")
            else:
                print("Error: Device connected is a Signal Generator. \nPlease, connect a Spectrum Analyzer")
    else:
        print("Not Connected")
except Exception as obEx:
    print("Error: " + str(obEx))


FREQ=np.array(FreqCH).flatten()
PowerCH0=[np.array(PowerCH0).flatten()]
PowerCH1=[np.array(PowerCH1).flatten()]
POWER= np.concatenate([PowerCH0,PowerCH1])
    
NAMEOUT= './data/Freq_Sweep_2Channels-'+Now
DATA=np.array([FREQ,POWER])
np.save(NAMEOUT,DATA)
print("Data saved in file:"+NAMEOUT )


#---------------------------------------------------------
# Close object and release resources
#---------------------------------------------------------
#print('Time between CH0 and CH1 (s):',np.round(T2-T1,3))

objRFE.Close()    #Finish the thread and close port
objRFE = None
Tf=time.time()
print('Total time (s):',np.round(Tf-Ti,3))
