import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import scipy.integrate as integrate
from matplotlib.ticker import AutoMinorLocator
from matplotlib.ticker import NullFormatter
from metodos import cm2inch

mpl.rcParams['legend.fontsize'] = 12
mpl.rcParams['axes.labelsize'] = 12
mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['ytick.labelsize'] = 12
mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['mathtext.fontset'] = 'dejavusans'
mpl.rcParams.update({'font.size': 12})

fig = plt.figure(figsize=(cm2inch(18),cm2inch(8)))
grid = plt.GridSpec(1, 1, wspace=0.25, hspace=0.2,
                    left=0.1,right=0.97,bottom=0.17,top=0.9)

ax = fig.add_subplot(grid[0,0])
ax.tick_params(bottom=True,top=True,right=True,direction='in',which='both')
ax.set_title(label='', y=0.1, loc='center')

f='Freq_Sweep_2Channels-08-16-23_16_03_54.npy'
data=np.load(f)
x=np.swapaxes(data,0,1)

#print(x[0])

C1=x[0]
C2=x[1]


for i in np.arange(0,len(C1)):
    C1_x=C1[i][0]
    C1_y=C1[i][1]
    C2_x=C2[i][0]
    C2_y=C2[i][1]
    print(C1_x,C1_y)
    ax.plot(C1_x,C1_y,drawstyle='steps-pre',lw=1.5)
    ax.plot(C2_x,C2_y,drawstyle='steps-pre',lw=1.5)

#ax.set_xticks(np.arange(0,600,100))
#ax.set_xlim(0,520)
#ax.set_ylim(0,3.3)
ax.set_ylabel('$I_\gamma$\,($10^3$ cuentas/cnl)')
ax.set_xlabel(r'$E_\gamma$(cnl)',labelpad=0)

ax.yaxis.set_minor_locator(AutoMinorLocator(2))
ax.xaxis.set_minor_locator(AutoMinorLocator(5))

ax.grid(which='both',alpha=0.1)
#leg=ax.legend(frameon=False,labelspacing=0.3,ncol=4,loc=1)
#for legobj in leg.legendHandles:
 #   legobj.set_linewidth(2.5)

ax.legend(frameon=False,labelspacing=0)

plt.savefig('RF_signals.pdf')
plt.show()
plt.close()
