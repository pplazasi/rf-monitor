#########
#para que 'import metodos_naei' funcione debe incluir el directorio en el que está
# este archivo (~/lenguajes/python) en el PATH de PYTHON. En mi computador:
# >export PYTHONPATH=$PYTHONPATH:/home/fernando/lenguajes/python
#
import pandas as pd
import numpy as np
from scipy.optimize import curve_fit

def cm2inch(cm):
    return cm/2.54

def fitRecta(x,y,sgm):
    b0 = (y[0]+y[-1])/2.
    b1 = (y[-1]-y[0])/(x[-1]-x[0])
    popt, pcov = curve_fit(lambda x, b0, b1 : recta(x,b0,b1),x,y,sigma=sgm)
    return popt, pcov

def stats_recta_naei(popt,pcov):
    b0 = popt[0] 
    stdv_b0 = np.sqrt(pcov[0,0])
    
    b1 = popt[1]*1e3
    stdv_b1 = np.sqrt(pcov[1,1])*1e3
    
    print('b0(c*1e3)= {0:4.2f} +- {1:2.2f}'.format(b0,stdv_b0))
    print('b1(c/cm)= {0:5.0f} +- {1:2.0f}'.format(b1,stdv_b1))

def exponencial(x,I_0,mu):
    return I_0 * np.exp(-mu*x)

def fitExponencial(x,y,sigmay):
    I0 = y[0]
    mu = np.log(y[0]/y[-1])/x[-1]
    p0 = np.array([I0, mu])
    popt, pcov = curve_fit(exponencial,x,y,p0,sigma=sigmay)
    return popt, pcov

def gauss(x,mu,stdv,amp):
    z = (mu-x)/stdv
    return amp*np.exp(-0.5*z**2)

def gaussNormd(x,mu,stdv):
    z = (mu-x)/stdv
    amp = 1/(np.sqrt(2*np.pi)*stdv)
    return amp*np.exp(-0.5*z**2)

def recta(x,b0,b1,x0):
    return b0 + b1 * (x-x0)

def gaussBckgrnd(x,mu,stdv,amp,b0,b1):
    z = (mu-x)/stdv
    gauss = amp*np.exp(-0.5*z**2)
    fondo = b0 + b1 * (x - mu)
    return gauss + fondo

def fitGaussBckgrnd(x,y,ncrt):
    b0 = (y[0]+y[-1])/2.0
    b1 = (y[-1]-y[0])/(x[-1]-x[0])
    yb = y[0] + b1*(np.array(x)-x[0])
    diff = y-yb
    mean = diff.mean()
    if mean < 0:
        mu = x[np.argmin(np.array(diff))]
        M = min(y)-b0
    else:
        mu = x[np.argmax(np.array(diff))]
        M = max(y)-b0

    sgm = (x[-1] - x[0]) / 9.0
    p0 = np.array([mu,sgm,M,b0,b1])    
    popt, pcov = curve_fit(gaussBckgrnd,x,y,p0,sigma=ncrt)
    return popt, pcov

def fitGaussBckgrndSigma1(x,y):
    b0 = (y[0]+y[-1])/2
    b1 = (y[-1]-y[0])/(x[-1]-x[0])
    yb = y[0] + b1*(np.array(x)-x[0])
    diff = y-yb
    mean = diff.mean()
    if mean < 0:
        mu = x[np.argmin(np.array(y))]
        M = min(y)-b0
    else:
        mu = x[np.argmax(np.array(y))]
        M = max(y)-b0

    s = (x[-1] - x[0]) / 9
    p0 = np.array([mu,s,M,b0,b1])    
    popt, pcov = curve_fit(gaussBckgrnd,x,y,p0)
    return popt, pcov

def statsGauss_tabla(popt,pcov):
    mu = popt[0] 
    stdv_mu = np.sqrt(pcov[0,0])
    
    s = popt[1]
    stdv_s = np.sqrt(pcov[1,1])
    
    M = popt[2]
    stdv_M = np.sqrt(pcov[2,2])
    
    b0 = popt[3]
    stdv_b0 = np.sqrt(pcov[3,3])
    
    b1 = popt[4]
    stdv_b1 = np.sqrt(pcov[4,4])

    print('mu(cnl)      s(cnl)      M(cts)        b0(cts)     b1(cts/c)')
    print('{0:4.1f}({1:2.1f}) &{2:3.1f}({3:2.1f}) &{4:4.2f}({5:2.2f}) &{6:4.2f}({7:2.2f}) &{8:4.3f}({9:1.3f}) \\\\'.format(mu,stdv_mu,s,stdv_s,M,stdv_M,b0,stdv_b0, b1,stdv_b1))


def tandem(x,mu1,stdv1,amp1,mu2,stdv2,amp2,b0,b1):
    z1 = 0.5 * (mu1-x)/stdv1
    gauss1 = amp1*np.exp(-z1**2)
    z2 = 0.5 * (mu2-x)/stdv2
    gauss2 = amp2*np.exp(-z2**2)
    fondo = b0 + b1 * (x - mu1)
    return gauss1 + gauss2 + fondo

def stats_rectaGauss_naei(popt,pcov):
    mu = popt[0] 
    stdv_mu = np.sqrt(pcov[0,0])
    
    s = popt[1]
    stdv_s = np.sqrt(pcov[1,1])
    
    Y = popt[2]
    stdv_Y = np.sqrt(pcov[2,2])
    
    b0 = popt[3]
    stdv_b0 = np.sqrt(pcov[3,3])
    
    b1 = popt[4]
    stdv_b1 = np.sqrt(pcov[4,4])

    Yb = np.abs(Y)/b0*100.
    stdv_Yb = Yb * np.sqrt( (stdv_Y/Y)**2 + (stdv_b0/b0)**2 )
    print('mu(cm)= {0:4.1f}  +- {1:2.1f}'.format(mu,stdv_mu))
    print(' s(cm)= {0:3.1f}  +- {1:2.1f}'.format(s,stdv_s))
    print(' Y(c*1e3)= {0:4.1f}  +- {1:2.1f}'.format(Y,stdv_Y))
    print(' Y(%b0)= {0:4.1f}  +- {1:2.1f}'.format(Yb,stdv_Yb))
    print('b0(c*1e3)= {0:4.2f}  +- {1:2.2f}'.format(b0,stdv_b0))
    print('b1(c/cm)= {0:4.0f}  +- {1:2.0f}'.format(b1*1e3,stdv_b1*1e3))
    
def triplete(x,mu1,sigma1,amp1,mu2,sigma2,amp2,mu3,sigma3,amp3,b0,b1):
    z1 = 0.5*((x-mu1)/sigma1)**2
    z2 = 0.5*((x-mu2)/sigma2)**2
    z3 = 0.5*((x-mu3)/sigma3)**2
    g1 = amp1*np.exp(-z1)
    g2 = amp2*np.exp(-z2)
    g3 = amp3*np.exp(-z3)
    return  g1 + g2 + +g3 + b0 + b1*(x-mu2)

#######Software para análisis de fluctuaciones
# Construye dos matrices, A, B, con las correspondientes lecturas
# de los archivos listados en 'filename'. Cada columna es un experimento.
# Formato de los archivos:
# Comentario               <- línea 0
# x(cm),A(c),B(c)          <- línea 1
# ...., ..., ...
# hdr = ordinal de la línea en la que está la descripción de las columnas

def buildAB(filename, hdr):
    f = pd.read_csv(filename[0],header = hdr)
    x = f['x(cm)'].to_numpy()
    A = f['A'].to_numpy().reshape(-1,1)
    B = f['B'].to_numpy().reshape(-1,1)
    for i in np.arange(1,len(filename)):
        A, B = concat2Reps(A, B, filename[i], hdr)
    return x, A, B

# Agrega una columna a  A y B con los datos del archivo filename 
def concat2Reps(A, B, filename, hdr):
    f = pd.read_csv(filename,header = hdr)
    A1 = f['A'].to_numpy().reshape(-1,1)
    B1 = f['B'].to_numpy().reshape(-1,1)
    A = np.concatenate((A,A1),axis=1)
    B = np.concatenate((B,B1),axis=1)
    return A, B

# Cálculo del promedio de las diferencias absolutas y de sus incertidumbres
def dXabs(X,repets,pares,puntos):
    dXs = np.zeros(puntos)
    sgdXs = np.zeros(puntos)
    for i in np.arange(0,repets):
        X1 = X[:,i]
        for j in np.arange(i+1,repets):
            X2 = X[:,j]
            dX = np.abs(X1-X2)
            sgdX = np.abs(X1) + np.abs(X2)
            dXs = dXs + dX
            sgdXs = sgdXs + sgdX
    
    dX = dXs/float(pares)
    sgdX = np.sqrt(sgdXs/float(pares))
    return dX, sgdX

### Cálculo de desviaciones estándar =
### sqrt[suma(diferencias con el valor medio)**2]
def buildFrameAB(filename, hdr):
    f = pd.read_csv(filename[0],header = hdr)
    x = f['x(cm)'].to_numpy()
    A = f['A']
    B = f['B']
    for i in np.arange(1,len(filename)):
        A, B = concat2frames(A, B, filename[i], hdr)

    meanA = A.mean(axis=1)
    stdA = A.std(axis=1)

    meanB = B.mean(axis=1)
    stdB = B.std(axis=1)

    return x, A, meanA, stdA, B, meanB, stdB

# Agrega una columna a  A y B con los datos del archivo filename 
def concat2frames(A, B, filename, hdr):
    f = pd.read_csv(filename,header = hdr)
    A1 = f['A']
    B1 = f['B']
    A = pd.concat([A,A1],axis=1)
    B = pd.concat([B,B1],axis=1)
    return A, B
