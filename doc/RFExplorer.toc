\babel@toc {spanish}{}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Hardware}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}RF Explorer 3G+ IoT for Raspberry Pi}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Raspberry Pi 4 Model B}{3}{section.2.2}%
\contentsline {section}{\numberline {2.3}Antenna SKALA-v2}{5}{section.2.3}%
\contentsline {section}{\numberline {2.4}Solid state switch}{6}{section.2.4}%
\contentsline {section}{\numberline {2.5}Bias tee}{7}{section.2.5}%
\contentsline {chapter}{\numberline {3}Software}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}RFETouch: Raspbian OS image for Raspberry Pi 4B}{9}{section.3.1}%
\contentsline {section}{\numberline {3.2}RFExplorer Python library}{10}{section.3.2}%
\contentsline {section}{\numberline {3.3}RF Monitor's custom code}{10}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Initialization}{11}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Connecting to RF Explorer}{13}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3}Setting up the measurement process}{14}{subsection.3.3.3}%
\contentsline {subsection}{\numberline {3.3.4}Acquiring RF data}{14}{subsection.3.3.4}%
\contentsline {subsection}{\numberline {3.3.5}Data processing and saving}{16}{subsection.3.3.5}%
\contentsline {subsection}{\numberline {3.3.6}Cleanup}{16}{subsection.3.3.6}%
\contentsline {chapter}{\numberline {4}Results}{17}{chapter.4}%
\contentsline {section}{\numberline {4.1}Test with signal generator}{17}{section.4.1}%
\contentsline {section}{\numberline {4.2}Test splitting the signal with FieldFox}{17}{section.4.2}%
