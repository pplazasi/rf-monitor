\chapter{Software}

This chapter provides a detailed guide on configuring the RF Monitor's software. It covers the installation of the OS image on the Raspberry Pi and the setup of the RF Explorer Python Library.

\section{RFETouch: Raspbian OS image for Raspberry Pi 4B}

This OS image is a modified version of the official Raspbian OS distribution. It includes some required features for easy integration with RF Explorer 3G+ IoT modules:

\begin{itemize}
    \item Python 3.5.2 installed.
    \item Disabled Wifi and Bluetooth - they cannot be active near to a sensitive IoT spectrum analyzer module as otherwise will produce interference and false readings.
    \item Optimized Raspbian distribution for IoT configuration, removed games and office tools.
    \item Enabled SSH.
    \item Designed for 8GB or larger micro-SD card.
\end{itemize}

The installation process is as it follows:

\begin{enumerate}
    \item The OS image can be downloaded from \href{http://j3.rf-explorer.com/downloads#iot}{here}, under the section ``Software for RF Explorer IoT modules''.
    \item The image needs to uploaded into a SD Card using Etcher, which can be downloaded from \href{https://etcher.balena.io}{here}.
    \item Insert SD Card in the Raspberry Pi and power it on. User and password are default $<$pi, raspberry$>$ as in any Raspbian setup. RFETouch always run as superuser as otherwise Raspberry Pi pins cannot be switched as normal user.
\end{enumerate}


The RFETouch Raspbian OS comes bundled with the RF Explorer Touch application, which provides real-time RF spectrum visualization. While this is a highly capable and complete application, it is not suitable for the requirements of the project. Consequently, it became necessary to develop a custom code capable of conducting RF spectrum measurements.

The application is configured to automatically start after the Raspbian GUI is loaded. While the application is on, the RF monitor is unable to perform other RF measurements simultaneously. As a result, the custom code cannot be run, and it becomes essential to disable this startup behavior. This can be accomplished locating the autostart configuration file, which may be in one of the following locations, depending on Raspberry Pi model:

\begin{itemize}
    \item /home/pi/.config/lxsession/LXDE-pi/autostart
    \item /etc/xdg/lxsession/LXDE-pi/autostart
\end{itemize}

Edit this file using \texttt{sudo nano} or equivalent editor as superuser, and remove this line:

\begin{verbatim}
    -e sudo /home/pi/RFExplorer/RFETouch
\end{verbatim}

\section{RFExplorer Python library}

RF Explorer library for Python uses class structure, with optimized class implementation and functionality. It can be easily installed using PIP:

\begin{verbatim}
    sudo pip3 install RFExplorer
\end{verbatim}

In case it needs to be upgraded:

\begin{verbatim}
    sudo pip3 install RFExplorer --upgrade
\end{verbatim}

After installing the library with pip, you just need to do an

\begin{verbatim}
    import RFExplorer
\end{verbatim}

to include RF Explorer functionality in your python script.

\section{RF Monitor's custom code}

The script file can be found in the next location in the RPi:

\begin{verbatim}
    /Documents/RFExplorer/RFE_Freq_2channels.py
\end{verbatim}

The script can be executed remotely on a different computer or network by first SSHing into the Raspberry Pi. The SSH credentials are as follows: username 'pi,' IP address '169.254.148.89,' and the password is 'radio'.
Figure \ref{F:ssh} shows an example of how to do it.

\begin{figure}[H]
    \centering
    \includegraphics[width=10cm]{../SSH.png}
    \caption{Example of how to execute the script after SSHing into the Raspberry Pi.}
    \label{F:ssh}
\end{figure}

The code can de divided in different processes: 

\begin{itemize}
    \item Initialization
    \item Connecting to the RF Explorer
    \item Setting up the measurement process
    \item Acquiring RF data
    \item Data processing and saving
    \item Cleanup
\end{itemize}

\begin{table}[]
    \centering
    \begin{tabular}{|c|c|}
    \hline
    \textbf{Process} & \textbf{Duration (seconds)} \\ \hline
    Connecting to RF Explorer & 6.117 \\ \hline
    \begin{tabular}[c]{@{}c@{}}Setting up measurement\\ (Top channel)\end{tabular} & 2.204 \\ \hline
    \begin{tabular}[c]{@{}c@{}}Acquiring RF Data\\ (Top channel)\end{tabular} & 3.042 \\ \hline
    Switching between channels & 1.008 \\ \hline
    \begin{tabular}[c]{@{}c@{}}Setting up measurement\\ (Bottom channel)\end{tabular} & 2.205 \\ \hline
    \begin{tabular}[c]{@{}c@{}}Acquiring RF Data\\ (Bottom channel)\end{tabular} & 3.04 \\ \hline
    Saving Results & 0.042 \\ \hline
    Cleanup & 2.305 \\ \hline
    Total duration & 19.9 \\ \hline
    \end{tabular}
    \caption{Timetable of the processes in the code.}
    \label{tab:timetable}
    \end{table}

Each process is described next, providing a description for each relevant line or command.

\subsection{Initialization}

In this section, the initial setup of the script is performed, configuring the necessary variables and objects for its operation.

Here the serial port identifier is set to None, this allows to autodetect the port:

\begin{verbatim}
    SERIALPORT = None
\end{verbatim}

The baud rate is the communication speed with the RF Explorer. The chosen value is supported by the device:

\begin{verbatim}
    BAUDRATE = 500000
\end{verbatim}

Definition set to initialize the object:

\begin{verbatim}
    objRFE = RFExplorer.RFECommunicator()
\end{verbatim}

By setting objRFE.AutoConfigure to False, the code is explicitly instructing the RF Explorer not to perform automatic configuration. Instead, it indicates that the configuration settings will be manually specified in the script:

\begin{verbatim}
    objRFE.AutoConfigure = False
\end{verbatim}

These are the initialize settings. The RF Explorer can perform data sweeps with a span size of maximum 150 MHz. These can be changed depending on what frequency range needs to be measured.

\begin{verbatim}
    SPAN_SIZE_MHZ =  150    
    START_SCAN_MHZ = 50
    STOP_SCAN_MHZ = 500
\end{verbatim}


The current date and time is defined. This will be used to save the data file later:

\begin{verbatim}
    Today= datetime.now()
    Now=Today.strftime("%m-%d-%y_%H:%M:%S")
\end{verbatim}
    



\subsection{Connecting to RF Explorer}

This section involves connecting to the RF Explorer, configuring GPIO pins, and establishing communication.

Find and show valid serial ports:
\begin{verbatim}
    objRFE.GetConnectedPorts()
\end{verbatim}
   
Reset the GPIO2 and the GPIO3 pins to a ``high'' state:

\begin{verbatim}
    objRFE.ResetIOT_HW(True)
\end{verbatim}
    
A loop is used to iterate through two channels: 0 and 1. These represent a “low” and “high” state of the GPIO12 pin:

\begin{verbatim}
for i in np.arange(0, 2):
\end{verbatim}

Upon activation, the GPIO12 pin switches to a ``low'' state (\texttt{i=0}), initiating data sweep of the Top channel. Then it switches to “high” (\texttt{i=1}) to measure the Bottom channel.

\begin{verbatim}
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(32, GPIO.OUT)
    GPIO.output(32, int(i))
\end{verbatim}

This line attempts to establish a connection with the RF Explorer using the already defined serial port and baud rate:

\begin{verbatim}
    if (objRFE.ConnectPort(SERIALPORT, BAUDRATE)):
\end{verbatim}

It waits for the RF Explorer to notify that a reset has been completed:

\begin{verbatim}
    while (objRFE.IsResetEvent):
        pass
\end{verbatim}

This line introduces a 3 second delay to allow the RF Explorer to stabilize after the reset:

\begin{verbatim}
    time.sleep(3)
\end{verbatim}

This command sends a request to the RF Explorer device to provide its configuration data:

\begin{verbatim}
    objRFE.SendCommand_RequestConfigData()
\end{verbatim}

These lines wait to receive configuration and model details from the RF Explorer device, processing received strings in the process:

\begin{verbatim}
    while (objRFE.ActiveModel == RFExplorer.RFE_Common.eModel.MODEL_NONE):
        objRFE.ProcessReceivedString(True)
\end{verbatim}

This conditional statement checks if the connected device is an analyzer. This is necessary because the library is also used to work with signal generators:

\begin{verbatim}
    if (objRFE.IsAnalyzer()):
\end{verbatim}

\subsection{Setting up the measurement process}

In this section, the configuration for the RF measurement is prepared, including frequency range and span settings.

This code retrieves and sets the SpanSize, StartFreq, and StopFreq values using the ControlSettings function that was defined before:

\begin{verbatim}
    SpanSize, StartFreq, StopFreq = ControlSettings(objRFE)
\end{verbatim}

This conditional statement checks if the configuration settings are valid and allows the process to continue if they are:

\begin{verbatim}
    if (SpanSize and StartFreq and StopFreq):
\end{verbatim}

A loop is used to continuously perform measurements within the specified frequency range:

\begin{verbatim}
    while (True):
\end{verbatim}

These lines update the device configuration to the current frequency range and initialize the objSweep variable for storing measurement data:

\begin{verbatim}
    objRFE.UpdateDeviceConfig(StartFreq, StopFreq)
    objSweep = None
\end{verbatim}

\subsection{Acquiring RF data}

This section involves continuously acquiring RF data, processing it, and storing the results.

A loop waits for the RF device to provide a new configuration, indicating the start of a new measurement sweep. Then it processes received strings from the RF device:

\begin{verbatim}
    while (True):
        objRFE.ProcessReceivedString(True);
\end{verbatim}

This conditional statement checks if there is sweep data available for processing:

\begin{verbatim}
    if (objRFE.SweepData.Count > 0):
\end{verbatim}

This line retrieves the latest sweep data received from the RF Explorer and assigns it to the objSweep variable:

\begin{verbatim}
    objSweep = objRFE.SweepData.GetData(objRFE.SweepData.Count - 1)
\end{verbatim}

This line calls the PrintPeak function to extract frequency and power data from the received sweep data:

\begin{verbatim}
    Freq, Power = PrintPeak(objRFE)
\end{verbatim}

These conditional statements store the acquired power and frequency data in separate arrays based on the selected channel (0 or 1). The frequency data only needs to be stored once since it is identical for both channels:

\begin{verbatim}
    if i == 0:
        PowerCH0.append(Power)
        FreqCH.append(Freq)

    if i == 1:
        PowerCH1.append(Power)
\end{verbatim}

This conditional statement checks if the absolute difference between the start frequency (`objRFE.StartFrequencyMHZ`) and the current frequency (`StartFreq`) is less than or equal to 0.001 MHz. If this condition is met, the loop for acquiring RF data is exited:

\begin{verbatim}
    if (np.fabs(objRFE.StartFrequencyMHZ - StartFreq) <= 0.001):
        break
\end{verbatim}

These lines update the frequency range for the next measurement sweep. The `StartFreq` is set to the previous `StopFreq`, and the `StopFreq` is adjusted accordingly. If the `StopFreq` exceeds the maximum allowed frequency (`STOP\_SCAN\_MHZ`), it is capped. The loop continues until the `StartFreq` is greater than or equal to the `StopFreq`:

\begin{verbatim}
    StartFreq = StopFreq
    StopFreq = StartFreq + SpanSize

    if (StopFreq > STOP_SCAN_MHZ):
        StopFreq = STOP_SCAN_MHZ

    if (StartFreq >= StopFreq):
        break
\end{verbatim}

If the configuration settings for the RF measurement are incorrect, this line prints an error message indicating that the settings need to be changed:

\begin{verbatim}
    else:
        print("Error: settings are wrong.\nPlease, change and try again")
\end{verbatim}

If the connected device is identified as a signal generator rather than a spectrum analyzer, this line prints an error message suggesting that a spectrum analyzer should be connected:

\begin{verbatim}
    else:
        print("Error: Device connected is a Signal Generator. \nPlease,
        connect a Spectrum Analyzer")
\end{verbatim}

If the RF device is not successfully connected, this line prints a message indicating that there is no connection:

\begin{verbatim}
    else:
        print("Not Connected")
\end{verbatim}


\subsection{Data processing and saving}

In this section, the acquired RF data is processed, organized, and saved to a file. 

This line flattens the list `FreqCH` into a one-dimensional NumPy array, which contains the frequency data acquired during the RF measurements:

\begin{verbatim}
    FREQ = np.array(FreqCH).flatten()
\end{verbatim}

Here, the lists `PowerCH0` and `PowerCH1` are flattened and enclosed in lists:

\begin{verbatim}
    PowerCH0 = [np.array(PowerCH0).flatten()]
    PowerCH1 = [np.array(PowerCH1).flatten()]
\end{verbatim}

This line concatenates the flattened power data arrays from channels 0 and 1 (`PowerCH0` and `PowerCH1`) into a single array called `POWER`. This combined array contains all the power data acquired during the RF measurements: 

\begin{verbatim}
    POWER = np.concatenate([PowerCH0, PowerCH1])
\end{verbatim}

The variable `NAMEOUT` is assigned a filename composed of 'Freq\_Sweep\_2Channels-' followed by the current date and time (captured earlier in the script as `Now`). This filename will be used to save the data:

\begin{verbatim}
    NAMEOUT = `Freq_Sweep_2Channels-' + Now
\end{verbatim}

This line creates a NumPy array `DATA` that contains both the frequency data (`FREQ`) and the power data (`POWER`) as its elements. This combines the acquired RF data into a single array:

\begin{verbatim}
    DATA = np.array([FREQ, POWER])
\end{verbatim}

The script uses the `np.save` function to save the `DATA` array to a .npy file with the name specified in `NAMEOUT`. This saves the acquired RF data for further analysis or reference:

\begin{verbatim}
    np.save(NAMEOUT, DATA)
\end{verbatim}

A message is printed to indicate that the RF data has been successfully saved in the specified file:

\begin{verbatim}
    print("Data saved in file:" + NAMEOUT)
\end{verbatim}

\subsection{Cleanup}

In this section, the script performs cleanup operations, including closing the communication object and releasing associated resources.

The `objRFE` object's `Close` method is called to finish the communication thread and close the serial port connection with the RF Explorer:

\begin{verbatim}
    objRFE.Close()   
\end{verbatim}

Finally, the `objRFE` object is set to `None` to release resources associated with the RF Explorer communication object:

\begin{verbatim}
    objRFE = None
\end{verbatim}





